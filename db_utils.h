#pragma once
#include <iostream>
#include <sstream>
#include "Database.h"
#include "procfile.h"


namespace insert {
    void proc(Database& db, int pid) {
        Query q (db, "INSERT INTO proc VALUES (?, ?, ?, ?);");

        std::string exe = getproc(pid, "exe");
        std::string cwd = getproc(pid, "cwd");
        std::string root = getproc(pid, "root");
        q.BindInt(pid);
        q.BindText(exe);
        q.BindText(cwd);
        q.BindText(root);
        q.Step();
    }

    void cmdline_args(Database& db, int pid) {
        Query q (db, "INSERT INTO cmdline_arg VALUES (?, ?, ?);");

        std::string _args = getproc(pid, "cmdline");
        std::string_view args (_args);
        dp(hd(args));
        size_t b = 0, e = -1;
        for (size_t i = 0; true; i++) {
            e = args.find('\0', b);
            if (e == std::string::npos) { break; }
            std::string_view arg = args.substr(b, e-b);
            b = e + 1;

            q.BindInt(pid);
            q.BindInt(i);
            q.BindText(arg);
            q.Step();
            q.ResetBind(false);
            q.Reset();
        }
    }

    void environ_vars(Database& db, int pid) {
        Query q (db, "INSERT INTO environ_var VALUES (?, ?, ?);");

        std::string _vars = getproc(pid, "environ");
        std::string_view vars (_vars);
        dp(hd(vars));
        size_t b = 0, e = -1;
        bool delme = false;
        for (size_t i = 0; true; i++) {
            e = vars.find('\0', b);
            if (e == std::string::npos) { break; }
            std::string_view keyval = vars.substr(b, e-b);
            b = e + 1;
            if (keyval.empty()) {
                if (!delme) {
                    std::cerr << "Found an empty envvar for " << pid << " at index " << i << std::endl;
                }
                delme = true;
                continue;
            }
            const auto sep = keyval.find('=');
            if (sep == std::string_view::npos) {
                std::cerr << "Found a no= envvar for " << pid << " at index " << i << std::endl;
                continue;
            }
            std::string_view key = keyval.substr(0, sep);
            std::string_view val = keyval.substr(sep+1);

            q.BindInt(pid);
            q.BindText(key);
            q.BindText(val);
            q.Step();
            q.ResetBind(false);
            q.Reset();
        }
    }

    void stat(Database& db, int pid) {
        Query q (db, "INSERT INTO stat VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");

        const std::string _args = getproc(pid, "stat");
        const std::string_view args (_args);
        dp(hd(args));
        size_t b = 0, e = 0, i;
        for (i = 0; e <= args.size(); i++) {
            e = args.find(' ', b);
            // if (e == std::string::npos) { break; }
            std::string_view arg = args.substr(b, e-b);
            dp(i);
            dp(arg);
            if (i == 1) {
                while (args.at(e-1) != ')') {
                    e = args.find(' ', e+1);
                    dp(args.at(e-1));
                    dp(args.at(e));
                    dp(args.at(e+1));
                }
                arg = args.substr(b, e-b);
                dp("Fixed arg");
                dp(arg);
                b = e + 1;
                q.BindText(arg);
            }
            b = e + 1;

            if (i == 2) {
                q.BindText(arg);
            } else if (i != 1) {
                if (i == 0) {
                    auto _pid = std::stoll(std::string(arg));
                    if (pid != _pid) {
                        std::cerr << "On pid " << pid << " stat _pid is " << _pid << std::endl;
                    }
                }
                q.BindInt64(std::stoull(std::string(arg)));
            }
        }
        if (i != 52) {
            std::cerr << "On pid " << pid << " stat expected 52 values, got " << i << std::endl;
            return;
        }
        q.Step();
    }

    void fd(Database& db, int pid) {
        Query q (db, "INSERT INTO fd VALUES (?, ?, ?);");

        for (auto& e : std::filesystem::directory_iterator(getprocpath(pid, "fd"))) {
            dp(e);
            auto fd_path = e.path();
            std::string symlink_val = std::filesystem::read_symlink(fd_path);
            dp(symlink_val);
            q.BindInt(pid);
            q.BindInt(std::stoi(fd_path.filename()));
            q.BindText(symlink_val);
            q.Step();
            q.ResetBind(false);
            q.Reset();
        }
    }

}

void print_rel(Database& db, std::string_view relname) {
    std::stringstream ss;
    ss << "SELECT * FROM " << relname << ";\0";
    dp(ss.str().data());
    dp(relname);
    auto r = db.Execute(ss.str().data());
    for (const auto& row : r) {
        for (const auto& col : row) {
            std::cout << col << '\t';
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void print_q_ret(Database& db, const char* sql) {
    auto r = db.Execute(sql);
    for (const auto& row : r) {
        for (const auto& col : row) {
            std::cout << col << '\t';
        }
        std::cout << std::endl;
    }
}

