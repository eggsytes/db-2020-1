#!/usr/bin/env bash
set -o errexit
SQL="$(readlink -f "$1")"
cd ..
clear
./build/bdtest < "$SQL"
