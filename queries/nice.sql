SELECT p.pid, p.exe, s.nice
FROM proc p JOIN stat s ON p.pid=s.pid
ORDER BY s.nice ASC
LIMIT 1;

