#pragma once
#include <sqlite3.h>
#include <vector>
#include <stdexcept>

class Database {
public:
    using table_t = std::vector<std::vector<std::string>>;
    using table_row_t = table_t::value_type;
    using table_cell_t = table_row_t::value_type;
private:
    friend class Query;
    sqlite3* db;
    static int callback(void* arg, int n, char** fields, char**) {
        table_t* out = (table_t*) arg;
        table_row_t row;
        for (int i = 0; i < n; i++) {
            row.emplace_back(fields[i]);
        }
        out->push_back(std::move(row));
        return 0;
    }
    void rc(int ret) {
        if (ret != SQLITE_OK) {
            throw std::runtime_error(sqlite3_errmsg(db));
        }
    }
public:
    Database(const char* filename) {
        rc(sqlite3_open(filename, &db));
    }
    ~Database() noexcept {
        sqlite3_close(db);
    }

    table_t Execute(const char* query) {
        table_t ret;
        rc(sqlite3_exec(db, query, &callback, &ret, nullptr) != SQLITE_OK);
        return ret;
    }
};

class Query {
    sqlite3_stmt* stmt;
    size_t bind_i = 1;
    Database& db;

    void rc(int ret) {
        if (ret != SQLITE_OK) {
            throw std::runtime_error(sqlite3_errmsg(db.db));
        }
    }
public:
    Query(Database& db, std::string_view sql) : db(db) {
        rc(sqlite3_prepare_v2(db.db, sql.data(), sql.size(), &stmt, 0));
    }
    ~Query() noexcept {
        rc(sqlite3_finalize(stmt));
    }

    void ResetBind(bool clear = true) {
        bind_i = 1;
        if (clear) {
            rc(sqlite3_clear_bindings(stmt));
        }
    }

    void BindText(std::string_view value) {
        rc(sqlite3_bind_text(
            stmt,
            bind_i++,
            value.data(),
            value.size(),
            SQLITE_STATIC
        ));
    }

    void BindInt(int value) {
        rc(sqlite3_bind_int(
            stmt,
            bind_i++,
            value
        ));
    }

    void BindInt64(sqlite3_int64 value) {
        rc(sqlite3_bind_int64(
            stmt,
            bind_i++,
            value
        ));
    }

    bool Step() {
        int ret = sqlite3_step(stmt);
        if (ret == SQLITE_DONE) { return false; }
        if (ret == SQLITE_ROW) { return true; }
        rc(1);  // Always throw
        return false;
    }

    void Reset() {
        rc(sqlite3_reset(stmt));
    }
};


