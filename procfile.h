#pragma once
#include <fstream>
#include <sstream>
#include <iterator>
#include <filesystem>

std::string readfile(const std::filesystem::path& path) {
    std::string s;
    auto f = std::ifstream(path, std::ios::binary);
    std::copy(
        std::istreambuf_iterator<char>(f),
        std::istreambuf_iterator<char>(),
        std::back_inserter(s)
    );
    return s;
}

std::filesystem::path getprocpath(int pid, std::string_view filename) {
    return std::filesystem::path("/proc") / std::to_string(pid) / filename;
}

std::string getproc(int pid, std::string_view filename) {
    namespace fs = std::filesystem;
    fs::path p = getprocpath(pid, filename);
    if (fs::is_symlink(p)) {
        return fs::read_symlink(p);
    } else {
        return readfile(p);
    }
}

