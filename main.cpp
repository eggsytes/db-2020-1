#define dp(a) ((void)(a))
#define hd(a) (a)
// #include <dbg.h>

#include <iostream>
#include <vector>
#include <filesystem>
#include <algorithm>
#include <cctype>
#include "Database.h"
#include "db_utils.h"

using namespace std;

bool is_pid(const std::filesystem::path& p) {
    std::string n = p.filename();
    return std::all_of(n.begin(), n.end(), [](auto c) { return std::isdigit(c); });
}

int main(int argc, char** argv) {
    std::vector<int> pids;
    if (argc > 1) {     // PIDs provided via args
        for (int i = 1; i < argc; i++) {
            const int pid = std::stoi(argv[i]);
            pids.push_back(pid);
        }
    } else {            // Find all PIDs automatically
        for (auto& e : std::filesystem::directory_iterator("/proc")) {
            if (!is_pid(e)) { continue; }
            // dp(e);
            const int pid = std::stoi(e.path().filename());
            pids.push_back(pid);
        }
    }
    std::string userQuery;
    while(cin) {
        std::string s;
        std::getline(cin, s);
        userQuery += "\n";
        userQuery += s;
        // } while(userQuery.back() != ';');
    }
    userQuery += "\0";


    // Database db ("test.db");
    Database db (":memory:");

    db.Execute(readfile("create.sql").c_str());

    constexpr bool showProgress = false;
    for (auto pid : pids) try {
        showProgress && std::cerr << "PID " << pid;
        insert::proc(db, pid);
        insert::cmdline_args(db, pid);
        insert::environ_vars(db, pid);
        insert::stat(db, pid);
        insert::fd(db, pid);
        showProgress && std::cerr << " +\n";
    } catch (const std::filesystem::filesystem_error& e) {
        showProgress && std::cerr << " -\n";
        if (e.code() == std::errc::permission_denied) {
            // std::cerr << "Premission denied on PID " << pid << std::endl;
        } else if (e.code() == std::errc::no_such_file_or_directory) {

        } else {
            std::cerr << "Error on pid " << pid << " " << e.code() << std::endl;
            std::cerr << "e.what(): " << e.what() << std::endl;
            throw e;
        }
    }

    print_q_ret(db, userQuery.data());
}
