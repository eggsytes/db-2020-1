CREATE TABLE proc (
    pid           INTEGER        NOT  NULL,
    exe           VARCHAR(4096)  NOT  NULL,
    cwd           VARCHAR(4096)  NOT  NULL,
    root          VARCHAR(4096)  NOT  NULL,
    PRIMARY KEY (pid)
);

CREATE TABLE cmdline_arg (
    pid           INTEGER        NOT  NULL,
    pos           INTEGER        NOT  NULL,
    value         VARCHAR(4096)  NOT  NULL,
    PRIMARY KEY (pid, pos),
    FOREIGN KEY (pid) REFERENCES proc (pid) ON DELETE CASCADE
);

CREATE TABLE environ_var (
    pid           INTEGER        NOT  NULL,
    name          VARCHAR(4096)  NOT  NULL,
    value         VARCHAR(4096)  NOT  NULL,
    PRIMARY KEY (pid, name),
    FOREIGN KEY (pid) REFERENCES proc (pid) ON DELETE CASCADE
);

CREATE TABLE fd (
    pid           INTEGER        NOT  NULL,
    handle        INTEGER        NOT  NULL,
    value         VARCHAR(4096)  NOT  NULL,
    PRIMARY KEY (pid, handle),
    FOREIGN KEY (pid) REFERENCES proc (pid) ON DELETE CASCADE
);

CREATE TABLE stat (
    pid           INTEGER        NOT  NULL,
    tcomm         VARCHAR(4096)  NOT  NULL,
    state         VARCHAR(1)     NOT  NULL,
    ppid          INTEGER        NOT  NULL,
    pgrp          INTEGER        NOT  NULL,
    sid           INTEGER        NOT  NULL,
    tty_nr        INTEGER        NOT  NULL,
    tty_pgrp      INTEGER        NOT  NULL,
    flags         INTEGER        NOT  NULL,
    min_flt       INTEGER        NOT  NULL,
    cmin_flt      INTEGER        NOT  NULL,
    maj_flt       INTEGER        NOT  NULL,
    cmaj_flt      INTEGER        NOT  NULL,
    utime         INTEGER        NOT  NULL,
    stime         INTEGER        NOT  NULL,
    cutime        INTEGER        NOT  NULL,
    cstime        INTEGER        NOT  NULL,
    priority      INTEGER        NOT  NULL,
    nice          INTEGER        NOT  NULL,
    num_threads   INTEGER        NOT  NULL,
    it_real_value INTEGER        NOT  NULL,
    start_time    INTEGER        NOT  NULL,
    vsize         INTEGER        NOT  NULL,
    rss           INTEGER        NOT  NULL,
    rsslim        INTEGER        NOT  NULL,
    start_code    INTEGER        NOT  NULL,
    end_code      INTEGER        NOT  NULL,
    start_stack   INTEGER        NOT  NULL,
    esp           INTEGER        NOT  NULL,
    eip           INTEGER        NOT  NULL,
    pending       INTEGER        NOT  NULL,
    blocked       INTEGER        NOT  NULL,
    sigign        INTEGER        NOT  NULL,
    sigcatch      INTEGER        NOT  NULL,
    _unused1      INTEGER        NOT  NULL,
    _unused2      INTEGER        NOT  NULL,
    _unused3      INTEGER        NOT  NULL,
    exit_signal   INTEGER        NOT  NULL,
    task_cpu      INTEGER        NOT  NULL,
    rt_priority   INTEGER        NOT  NULL,
    policy        INTEGER        NOT  NULL,
    blkio_ticks   INTEGER        NOT  NULL,
    gtime         INTEGER        NOT  NULL,
    cgtime        INTEGER        NOT  NULL,
    start_data    INTEGER        NOT  NULL,
    end_data      INTEGER        NOT  NULL,
    start_brk     INTEGER        NOT  NULL,
    arg_start     INTEGER        NOT  NULL,
    arg_end       INTEGER        NOT  NULL,
    env_start     INTEGER        NOT  NULL,
    env_end       INTEGER        NOT  NULL,
    exit_code     INTEGER        NOT  NULL,
    PRIMARY KEY (pid),
    FOREIGN KEY (pid) REFERENCES proc (pid) ON DELETE CASCADE
);

