#!/usr/bin/env bash
set -o errexit
ninja -C build/
rm -f test.db
build/bdtest 1238 2>&1 1>/dev/null
